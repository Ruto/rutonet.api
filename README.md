# RutoNet.API

[![pipeline status](https://gitlab.com/Ruto/rutonet.api/badges/master/pipeline.svg)](https://gitlab.com/Ruto/rutonet.api/-/commits/master)

* Trying stuff with [CQRS](https://martinfowler.com/bliki/CQRS.html) and [Clean Architecture](https://github.com/ardalis/CleanArchitecture) (hopefully)

## Technologies
* [MediatR](https://github.com/jbogard/MediatR) - Mediator for our CQRS pattern
* [EFCore.NamingConventions](https://github.com/efcore/EFCore.NamingConventions) - Apply snake-casing to EFCore
* [Npgsql(EFCore)](http://www.npgsql.org/efcore/index.html) - Postgres driver with their EFCore package
* [NodaTime (Npgsql)](http://www.npgsql.org/efcore/mapping/nodatime.html) - Date/Time object encouraged by Npgsql
* [AutoMapper](https://automapper.org/) - Map between Entity/ViewModel/DTOs
* [Serilog](https://serilog.net/) - LOGS EVERYWHERE
* [Docker](https://www.docker.com/) - CONTAINERS
* [AWS S3](https://aws.amazon.com/sdk-for-net/) - Upload replays to S3