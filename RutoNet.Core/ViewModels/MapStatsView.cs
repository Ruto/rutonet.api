using System;
using System.Collections.Generic;

namespace RutoNet.Core.ViewModels
{
    public class MapStatsView
    {
        public string Map { get; set; }
        public Dictionary<string, int> Progression { get; set; }
    }
}