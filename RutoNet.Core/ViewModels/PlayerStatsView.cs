using System.Collections.Generic;

namespace RutoNet.Core.ViewModels
{
    public class PlayerStatsView
    {
        public string SteamID64 { get; set; }
        public PlayerView Info { get; set; }
        public int TotalRuns { get; set; }
        public List<MapFinishView> MapFinishes { get; set; }
    }
}