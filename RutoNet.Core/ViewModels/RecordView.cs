﻿using System;

namespace RutoNet.Core.ViewModels
{
    public class RecordView
    {
        public int Id { get; set; }
        public string SteamID64 { get; set; }
        public string PlayerName { get; set; }
        public string Map { get; set; }
        public string Mode { get; set; }
        public int Course { get; set; }
        public int Time { get; set; }
        public int Teleports { get; set; }
        public DateTime Created { get; set; }
    }
}