using System;

namespace RutoNet.Core.ViewModels
{
    public class MapView
    {
        public string Name { get; set; }
        public int ReplayCount { get; set; }
        public DateTime Created { get; set; }
    }
}