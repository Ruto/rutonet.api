using System.Collections.Generic;

namespace RutoNet.Core.ViewModels
{
    public class RecordsResultView
    {
        public int Offset { get; set; }
        public int Count { get; set; }
        public int Total { get; set; }
        public List<RecordView> Records { get; set; }
    }
}