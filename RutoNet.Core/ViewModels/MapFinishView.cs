using System;

namespace RutoNet.Core.ViewModels
{
    public class MapFinishView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalRecords { get; set; }
        public DateTime LastPlayed { get; set; }
    }
}