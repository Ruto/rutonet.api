using NodaTime;
using RutoNet.Core.Entities.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RutoNet.Core.Entities
{
    public class Record
    {
        [Key]
        public int Id { get; set; }

        public long SteamID64 { get; set; }

        [Required]
        public string MapName { get; set; }

        public GokzMode Mode { get; set; }
        public int Course { get; set; }
        public long Time { get; set; }
        public int Teleports { get; set; }
        public Instant Created { get; set; }

        [ForeignKey("MapName")]
        public Map Map { get; set; }

        [ForeignKey("SteamID64")]
        public Player Player { get; set; }
    }
}