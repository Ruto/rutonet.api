using NodaTime;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RutoNet.Core.Entities
{
    public class Map
    {
        [Key]
        public string Name { get; set; }

        public int ReplayCount { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Instant Created { get; set; }
    }
}