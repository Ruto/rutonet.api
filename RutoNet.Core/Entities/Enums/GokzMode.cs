namespace RutoNet.Core.Entities.Enums
{
    public enum GokzMode
    {
        Vanilla = 0,
        SimpleKZ = 1,
        KZTimer = 2
    }
}