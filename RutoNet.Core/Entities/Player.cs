using NodaTime;
using System.ComponentModel.DataAnnotations;

namespace RutoNet.Core.Entities
{
    public class Player
    {
        [Key]
        public long SteamID64 { get; set; }

        public string Name { get; set; }
        public Instant Created { get; set; }
        public Instant Updated { get; set; }
    }
}