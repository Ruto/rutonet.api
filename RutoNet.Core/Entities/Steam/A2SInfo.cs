using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RutoNet.Core.Entities.Steam
{
    public class A2SInfo
    {
        public string IP { get; set; }
        public byte Header { get; set; }
        public byte Protocol { get; set; }
        public string Name { get; set; }
        public string Map { get; set; }
        public string Folder { get; set; }
        public string Game { get; set; }
        public short ID { get; set; }
        public byte Players { get; set; }
        public byte MaxPlayers { get; set; }
        public byte Bots { get; set; }
        public byte ServerType { get; set; }
        public byte Environment { get; set; }
        public byte Visibility { get; set; }
        public byte VAC { get; set; }
        public string Version { get; set; }
        public byte EDF { get; set; }
        public short Port { get; set; }
        public long SteamID { get; set; }
        public short SourceTVPort { get; set; }
        public string SourceTVName { get; set; }
        public string Keywords { get; set; }
        public long GameID { get; set; }

        public static A2SInfo Parse(byte[] data)
        {
            using var ms = new MemoryStream(data);
            using var br = new BinaryReader(ms);
            br.ReadByte();
            br.ReadByte();
            br.ReadByte();
            br.ReadByte();
            var a2s = new A2SInfo
            {
                Header = br.ReadByte(),
                Protocol = br.ReadByte(),
                Name = br.ReadUTF8String(),
                Map = br.ReadUTF8String(),
                Folder = br.ReadUTF8String(),
                Game = br.ReadUTF8String(),
                ID = br.ReadInt16(),
                Players = br.ReadByte(),
                MaxPlayers = br.ReadByte(),
                Bots = br.ReadByte(),
                ServerType = br.ReadByte(),
                Environment = br.ReadByte(),
                Visibility = br.ReadByte(),
                VAC = br.ReadByte(),
                Version = br.ReadUTF8String(),
                EDF = br.ReadByte()
            };
            if ((a2s.EDF & 0x80) != 0)
                a2s.Port = br.ReadInt16();
            if ((a2s.EDF & 0x10) != 0)
                a2s.SteamID = br.ReadInt64();
            if ((a2s.EDF & 0x40) != 0)
            {
                a2s.SourceTVPort = br.ReadInt16();
                a2s.SourceTVName = br.ReadUTF8String();
            }
            if ((a2s.EDF & 0x20) != 0)
                a2s.Keywords = br.ReadUTF8String();
            if ((a2s.EDF & 0x01) != 0)
                a2s.GameID = br.ReadInt64();
            return a2s;
        }
    }

    internal static class BinaryReaderExtension
    {
        public static string ReadUTF8String(this BinaryReader br)
        {
            var list = new List<byte>();
            byte b;
            while ((b = br.ReadByte()) != 0x00)
                list.Add(b);
            return Encoding.UTF8.GetString(list.ToArray()).Trim();
        }
    }
}