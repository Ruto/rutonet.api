using NodaTime;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RutoNet.Core.Entities
{
    public class PlayerMap
    {
        public long SteamID64 { get; set; }
        public string Map { get; set; }
        public int TotalRecords { get; set; }
        public int PbId { get; set; }
        public Instant Created { get; set; }
        public Instant Updated { get; set; }

        [ForeignKey("SteamID64")]
        public Player Player { get; set; }

        [ForeignKey("Map")]
        public Map MapLink { get; set; }
    }
}