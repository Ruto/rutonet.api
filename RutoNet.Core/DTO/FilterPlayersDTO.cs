namespace RutoNet.Core.DTO
{
    public class FilterPlayersDTO
    {
        public long? SteamID64 { get; set; }
        public string Name { get; set; }

        public int Limit { get; set; } = -1;
    }
}