namespace RutoNet.Core.DTO
{
    public class CreateRecordDTO
    {
        public long SteamID64 { get; set; }
        public string PlayerName { get; set; }
        public string MapName { get; set; }
        public int Mode { get; set; }
        public int Course { get; set; }
        public long Time { get; set; }
        public int Teleports { get; set; }
    }
}