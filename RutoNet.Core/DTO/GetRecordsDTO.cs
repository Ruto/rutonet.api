namespace RutoNet.Core.DTO
{
    public class GetRecordsDTO
    {
        public long? SteamID64 { get; set; }
        public string[] Mode { get; set; }
        public string Map { get; set; } = null;
        public int Limit { get; set; } = -1;
        public int Offset { get; set; } = -1;
    }
}