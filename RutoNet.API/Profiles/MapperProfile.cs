using AutoMapper;
using NodaTime;
using RutoNet.Core.DTO;
using RutoNet.Core.Entities;
using RutoNet.Core.ViewModels;
using System;

namespace RutoNet.API.Profiles
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<CreateMapDTO, Map>();
            CreateMap<CreateRecordDTO, Record>();

            CreateMap<Map, MapView>();
            CreateMap<Player, PlayerView>();
            CreateMap<Record, RecordView>()
                .ForMember(view => view.PlayerName, opt => opt.MapFrom(record => record.Player.Name))
                .ForMember(view => view.Mode, opt => opt.MapFrom(record => record.Mode.ToString("f")))
                .ForMember(view => view.Map, opt => opt.MapFrom(rec => rec.MapName));
               // .ForMember(view => view.Map, opt => opt.MapFrom(record => record.Map.Name));
            // .ForMember(view => view.SteamID64, opt => opt.MapFrom(record => record.SteamID64.ToString()));

            CreateMap<Instant, DateTime>().ConvertUsing(instant => instant.ToDateTimeUtc());
            CreateMap<long, string>().ConvertUsing(int64 => int64.ToString());
        }
    }
}