using MediatR;
using Microsoft.AspNetCore.Mvc;
using RutoNet.Core.DTO;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Query;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RutoNet.API.Controllers
{
    [Route("api/[controller]")]
    public class PlayersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PlayersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Query all players
        /// </summary>
        /// <param name="dto">Filter parameters</param>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlayerView>>> GetAll([FromQuery] FilterPlayersDTO dto)
        {
            var query = new GetAllPlayersQuery(dto);
            var player = await _mediator.Send(query);
            return player;
        }

        /// <summary>
        /// Query a specific player
        /// </summary>
        /// <param name="steamid64">SteamID64 of the player</param>
        [HttpGet("id/{steamid64}")]
        public async Task<ActionResult<PlayerView>> GetById(long steamid64)
        {
            var query = new GetPlayerQuery(steamid64);
            var player = await _mediator.Send(query);
            return player.FirstOrDefault();
        }
    }
}