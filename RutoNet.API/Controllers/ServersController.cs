using MediatR;
using Microsoft.AspNetCore.Mvc;
using RutoNet.Core.Entities.Steam;
using RutoNet.Infrastructure.Query;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RutoNet.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ServersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ServersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Query all servers
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<A2SInfo>>> GetAll()
        {
            var query = new GetServersQuery();
            var serverInfos = await _mediator.Send(query);
            return serverInfos;
        }
    }
}