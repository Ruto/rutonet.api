using MediatR;
using Microsoft.AspNetCore.Mvc;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Command;
using System;
using System.IO;
using System.Threading.Tasks;

namespace RutoNet.API.Controllers
{
    [Route("api/[controller]")]
    public class PixelController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PixelController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Submit a record by submitting their GOKZ-format replay
        /// </summary>
        [HttpPost]
        [RequestSizeLimit(104857600)] // 100MB
        public async Task<ActionResult<RecordView>> SubmitReplay()
        {
            // Simple protection
            if (Request.Headers["X-Api-Key"] != "Kurisu")
                return Unauthorized();

            var command = new CreateReplayCommand(Request.Body);
            var record = await _mediator.Send(command);
            return record;
        }
    }
}