using MediatR;
using Microsoft.AspNetCore.Mvc;
using RutoNet.Infrastructure.Query;
using System.Threading.Tasks;

namespace RutoNet.API.Controllers
{
    [Route("api/[controller]")]
    public class StatsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public StatsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("player/{steamId64:long}")]
        public async Task<ActionResult> GetPlayerStats(long steamId64)
        {
            var query = new GetPlayerStatsQuery { SteamID64 = steamId64 };
            var stat = await _mediator.Send(query);
            return Ok(stat);
        }

        [HttpGet("map/{map}")]
        public async Task<ActionResult> GetMapResults(string map)
        {
            var query = new GetMapStatsQuery(map);
            var stats = await _mediator.Send(query);
            return Ok(stats);
        }
    }
}