using MediatR;
using Microsoft.AspNetCore.Mvc;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Query;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RutoNet.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MapsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MapsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Query all maps
        /// </summary>
        /// <param name="query">Filter maps based on given search term</param>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MapView>>> GetAll([FromQuery] string query)
        {
            var mapsQuery = new GetAllMapsQuery() { Term = query };
            var maps = await _mediator.Send(mapsQuery);
            return maps;
        }

        /// <summary>
        /// Query a specific map
        /// </summary>
        /// <param name="name">Specific name of the map</param>
        [HttpGet("{name}")]
        public async Task<ActionResult<MapView>> GetByName(string name)
        {
            var query = new GetMapQuery(name);
            var map = await _mediator.Send(query);
            return map;
        }
    }
}