using MediatR;
using Microsoft.AspNetCore.Mvc;
using RutoNet.Core.DTO;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Command;
using RutoNet.Infrastructure.Query;
using Serilog;
using System;
using System.IO;
using System.Threading.Tasks;

namespace RutoNet.API.Controllers
{
    [Route("api/[controller]")]
    public class RecordsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RecordsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Query records
        /// </summary>
        /// <param name="dto">Query parameters</param>
        [HttpGet]
        public async Task<ActionResult<RecordsResultView>> GetAll([FromQuery] GetRecordsDTO dto)
        {
            var query = new GetRecordsQuery(dto);
            var result = await _mediator.Send(query);
            return result;
        }

        /// <summary>
        /// Query a record
        /// </summary>
        /// <param name="id">The record id</param>
        [HttpGet("{id}")]
        public async Task<ActionResult<RecordView>> GetById(int id)
        {
            var query = new GetRecordQuery(id);
            var record = await _mediator.Send(query);
            return record;
        }

        /// <summary>
        /// Get a specific replay
        /// </summary>
        /// <param name="id">The record id</param>
        [HttpGet("{id}/replay")]
        public async Task<IActionResult> GetReplayById(int id)
        {
            var recordQuery = new GetRecordQuery(id);
            var record = await _mediator.Send(recordQuery);

            var query = new GetReplayQuery(record);
            var replay = await _mediator.Send(query);
            return File(replay, "application/octet-stream", $"{record.Map}_{record.Mode}_{record.SteamID64}_{record.Id}.replay");
        }

        /// <summary>
        /// Submit a record by submitting their GOKZ-format replay
        /// </summary>
        [HttpPost]
        [RequestSizeLimit(104857600)] // 100MB
        public async Task<ActionResult<RecordView>> SubmitReplay()
        {
            // Simple protection
            if (Request.Headers["X-Api-Key"] != "Kurisu")
                return Unauthorized();
            var command = new CreateReplayCommand(Request.Body);
            var record = await _mediator.Send(command);
            return record;
        }
    }
}