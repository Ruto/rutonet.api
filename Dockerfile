FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build
WORKDIR /app

# Add all projects
COPY RutoNet.API/*.csproj ./RutoNet.API/
COPY RutoNet.Core/*.csproj ./RutoNet.Core/
COPY RutoNet.Infrastructure/*.csproj ./RutoNet.Infrastructure/
COPY *.sln .
RUN [ "dotnet", "restore" ]

# Copy dependencies over
COPY . ./
WORKDIR /app/RutoNet.API
RUN dotnet publish -c Release -o out

# Copy from build for smaller image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.0-alpine
WORKDIR /app
COPY --from=build /app/RutoNet.API/out .

# Creds for S3 bucket
ENV AWS_ACCESS_KEY_ID key
ENV AWS_SECRET_ACCESS_KEY secret

ENTRYPOINT [ "dotnet", "RutoNet.API.dll" ]