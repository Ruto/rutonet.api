using MediatR;
using RutoNet.Core.DTO;
using RutoNet.Core.ViewModels;
using System.Collections.Generic;

namespace RutoNet.Infrastructure.Query
{
    public class GetAllPlayersQuery : IRequest<List<PlayerView>>
    {
        public FilterPlayersDTO Filter { get; set; }

        public GetAllPlayersQuery() {}

        public GetAllPlayersQuery(FilterPlayersDTO dto)
        {
            Filter = dto;
        }
    }
}