using MediatR;
using RutoNet.Core.ViewModels;

namespace RutoNet.Infrastructure.Query
{
    public class GetPlayerStatsQuery : IRequest<PlayerStatsView>
    {
        public long SteamID64 { get; set; }
    }
}