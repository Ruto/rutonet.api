using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Query
{
    public class GetPlayerHandler : IRequestHandler<GetPlayerQuery, List<PlayerView>>
    {
        private readonly PixelContext _context;
        private readonly IMapper _mapper;

        public GetPlayerHandler(PixelContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<PlayerView>> Handle(GetPlayerQuery request, CancellationToken cancellationToken)
        {
            var query = _context.Players.AsQueryable();
            if (request.SteamID64.HasValue)
                query = query.Where(player => player.SteamID64 == request.SteamID64.Value);
            else
                query = query.Where(player => EF.Functions.ILike(player.Name, $"%{request.Name}%"));
            var players = await query.ToListAsync(cancellationToken);
            return _mapper.Map<List<PlayerView>>(players);
        }
    }
}