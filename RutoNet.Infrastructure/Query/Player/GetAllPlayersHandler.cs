using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Query
{
    public class GetAllPlayersHandler : IRequestHandler<GetAllPlayersQuery, List<PlayerView>>
    {
        private readonly PixelContext _context;
        private readonly IMapper _mapper;

        public GetAllPlayersHandler(PixelContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<PlayerView>> Handle(GetAllPlayersQuery request, CancellationToken cancellationToken)
        {
            var query = _context.Players.AsQueryable();
            if (request.Filter.SteamID64.HasValue)
                query = query.Where(player => player.SteamID64 == request.Filter.SteamID64.Value);

            if (request.Filter.Limit > -1)
                query = query.Take(request.Filter.Limit);

            if (!string.IsNullOrEmpty(request.Filter.Name))
                query = query.Where(player => EF.Functions.ILike(player.Name, $"%{request.Filter.Name}%"));
            return _mapper.Map<List<PlayerView>>(await query.ToListAsync(cancellationToken));
        }
    }
}