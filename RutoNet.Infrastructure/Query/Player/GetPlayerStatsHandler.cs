using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Query
{
    public class GetPlayerStatsHandler : IRequestHandler<GetPlayerStatsQuery, PlayerStatsView>
    {
        private readonly PixelContext _context;
        private readonly IMapper _mapper;

        public GetPlayerStatsHandler(PixelContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PlayerStatsView> Handle(GetPlayerStatsQuery request, CancellationToken cancellationToken)
        {
            var view = new PlayerStatsView { SteamID64 = request.SteamID64.ToString() };
            view.Info = _mapper.Map<PlayerView>(await _context.Players.FindAsync(request.SteamID64));
            if (view.Info == null) return null;

            var mapStats = await _context.PlayerMapStats.Where(stat => stat.SteamID64 == request.SteamID64).ToListAsync();
            view.MapFinishes = mapStats.Select(stat => new MapFinishView
            {
                Id = stat.PbId,
                Name = stat.Map,
                TotalRecords = stat.TotalRecords,
                LastPlayed = stat.Updated.ToDateTimeUtc()
            }).ToList();
            view.TotalRuns = mapStats.Sum(stat => stat.TotalRecords);

            return view;
        }
    }
}