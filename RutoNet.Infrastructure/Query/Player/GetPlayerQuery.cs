using MediatR;
using RutoNet.Core.ViewModels;
using System.Collections.Generic;

namespace RutoNet.Infrastructure.Query
{
    public class GetPlayerQuery : IRequest<List<PlayerView>>
    {
        public long? SteamID64 { get; set; } = -1;
        public string Name { get; set; } = null;

        public GetPlayerQuery(long steamId64)
        {
            SteamID64 = steamId64;
        }

        public GetPlayerQuery(string name)
        {
            Name = name;
        }
    }
}