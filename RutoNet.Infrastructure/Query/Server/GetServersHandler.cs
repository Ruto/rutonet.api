using MediatR;
using Microsoft.Extensions.Caching.Memory;
using RutoNet.Core.Entities.Steam;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Query
{
    public class GetServersHandler : IRequestHandler<GetServersQuery, List<A2SInfo>>
    {
        private static readonly byte[] A2S_Query = { 0xFF, 0xFF, 0xFF, 0xFF, 0x54, 0x53, 0x6F, 0x75, 0x72, 0x63, 0x65, 0x20, 0x45, 0x6E, 0x67, 0x69,
            0x6E, 0x65, 0x20, 0x51, 0x75, 0x65, 0x72, 0x79, 0x00 };

        private static readonly List<KeyValuePair<string, short>> Servers = new List<KeyValuePair<string, short>>
        {
            new KeyValuePair<string, short>("104.194.11.3", 27015),
            new KeyValuePair<string, short>("104.194.11.3", 27115),
            new KeyValuePair<string, short>("104.194.11.3", 27215)
        };

        private readonly IMemoryCache _cache;

        public GetServersHandler(IMemoryCache cache)
        {
            _cache = cache;
        }

        public async Task<List<A2SInfo>> Handle(GetServersQuery request, CancellationToken cancellationToken)
        {
            var servers = await _cache.GetOrCreateAsync("server_info", async entry =>
            {
                var list = new List<A2SInfo>();
                foreach (var (ip, port) in Servers.Select(server => (server.Key, server.Value)))
                {
                    using var udpClient = new UdpClient();
                    udpClient.Client.SendTimeout = 200;
                    udpClient.Client.ReceiveTimeout = 200;
                    await udpClient.SendAsync(A2S_Query, A2S_Query.Length, new IPEndPoint(IPAddress.Parse(ip), port));
                    var result = await udpClient.ReceiveAsync();
                    var info = A2SInfo.Parse(result.Buffer);
                    info.IP = ip;
                    list.Add(info);
                }
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(30);
                return list;
            });
            return servers;
        }
    }
}