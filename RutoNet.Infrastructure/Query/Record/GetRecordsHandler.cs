using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RutoNet.Core.Entities.Enums;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Query
{
    public class GetRecordsHandler : IRequestHandler<GetRecordsQuery, RecordsResultView>
    {
        private readonly PixelContext _context;
        private readonly IMapper _mapper;

        public GetRecordsHandler(PixelContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<RecordsResultView> Handle(GetRecordsQuery request, CancellationToken cancellationToken)
        {
            var dto = request.GetRecordsDTO;
            var query = _context.Records.AsQueryable();
            var isMapFiltered = !string.IsNullOrEmpty(dto.Map);

            // Joins
            query = query.Include(rec => rec.Player);
            query = query.Include(rec => rec.Map);

            // Basic filters
            if (isMapFiltered)
                query = query.Where(rec => rec.Map.Name == dto.Map.ToLower());

            if (dto.SteamID64.HasValue)
                query = query.Where(rec => rec.SteamID64 == dto.SteamID64.Value);

            if (dto.Mode?.Length > 0)
            {
                var modes = new List<GokzMode>();
                foreach (var m in dto.Mode)
                {
                    if (Enum.TryParse(m, true, out GokzMode gokzMode))
                        modes.Add(gokzMode);
                }
                query = query.Where(rec => modes.Contains(rec.Mode));
            }

            query = isMapFiltered ? query.OrderBy(rec => rec.Time) : query.OrderByDescending(rec => rec.Created);

            var total = await query.CountAsync(cancellationToken);
            if (dto.Offset > -1)
                query = query.Skip(dto.Offset);
            if (dto.Limit > -1)
                query = query.Take(dto.Limit);

            var records = await query.ToListAsync(cancellationToken);

            var result = new RecordsResultView
            {
                Offset = Math.Max(dto.Offset, 0),
                Count = records.Count,
                Total = total,
                Records = _mapper.Map<List<RecordView>>(records)
            };
            return result;
        }
    }
}