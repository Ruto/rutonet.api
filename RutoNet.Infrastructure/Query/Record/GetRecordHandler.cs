﻿using AutoMapper;
using MediatR;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Query
{
    public class GetRecordHandler : IRequestHandler<GetRecordQuery, RecordView>
    {
        private readonly PixelContext _context;
        private readonly IMapper _mapper;

        public GetRecordHandler(PixelContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<RecordView> Handle(GetRecordQuery request, CancellationToken cancellationToken)
        {
            var record = await _context.Records.FindAsync(request.Id);
            return _mapper.Map<RecordView>(record);
        }
    }
}