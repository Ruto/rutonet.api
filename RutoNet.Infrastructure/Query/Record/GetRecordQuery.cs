﻿using MediatR;
using RutoNet.Core.ViewModels;

namespace RutoNet.Infrastructure.Query
{
    public class GetRecordQuery : IRequest<RecordView>
    {
        public int Id { get; set; }

        public GetRecordQuery(int id)
        {
            Id = id;
        }
    }
}