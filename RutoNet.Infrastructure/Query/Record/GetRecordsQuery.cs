using MediatR;
using RutoNet.Core.DTO;
using RutoNet.Core.ViewModels;

namespace RutoNet.Infrastructure.Query
{
    public class GetRecordsQuery : IRequest<RecordsResultView>
    {
        public GetRecordsDTO GetRecordsDTO { get; set; }

        public GetRecordsQuery(GetRecordsDTO dto)
        {
            GetRecordsDTO = dto;
        }
    }
}