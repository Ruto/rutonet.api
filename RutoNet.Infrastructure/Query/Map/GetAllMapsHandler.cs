using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Query
{
    public class GetAllMapsHandler : IRequestHandler<GetAllMapsQuery, List<MapView>>
    {
        private readonly PixelContext _context;
        private readonly IMapper _mapper;
        private readonly IMemoryCache _cache;

        public GetAllMapsHandler(PixelContext context, IMapper mapper, IMemoryCache cache)
        {
            _context = context;
            _mapper = mapper;
            _cache = cache;
        }

        public async Task<List<MapView>> Handle(GetAllMapsQuery request, CancellationToken cancellationToken)
        {
            var maps = await _cache.GetOrCreateAsync("all_maps_query", async entry =>
            {
                var query = _context.Maps.AsQueryable();
                if (!string.IsNullOrEmpty(request.Term))
                    query = query.Where(map => EF.Functions.Like(map.Name, $"%{request.Term.ToLower()}%"));
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(20);
                return _mapper.Map<List<MapView>>(await query.OrderBy(m => m.Name).ToListAsync(cancellationToken));
            });
            return maps;
        }
    }
}