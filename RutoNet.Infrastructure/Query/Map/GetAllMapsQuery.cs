using MediatR;
using RutoNet.Core.ViewModels;
using System.Collections.Generic;

namespace RutoNet.Infrastructure.Query
{
    public class GetAllMapsQuery : IRequest<List<MapView>>
    {
        public string Term { get; set; }
    }
}