using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Query
{
    public class GetMapHandler : IRequestHandler<GetMapQuery, MapView>
    {
        private readonly PixelContext _context;
        private readonly IMapper _mapper;

        public GetMapHandler(PixelContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<MapView> Handle(GetMapQuery request, CancellationToken cancellationToken)
        {
            var map = await _context.Maps.FirstOrDefaultAsync(map => EF.Functions.ILike(map.Name, $"%{request.Name}%"), cancellationToken);
            return _mapper.Map<MapView>(map);
        }
    }
}