using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Query
{
    public class GetMapStatsHandler : IRequestHandler<GetMapStatsQuery, MapStatsView>
    {
        private readonly PixelContext _context;
        private readonly IMapper _mapper;
        private readonly IMemoryCache _cache;

        public GetMapStatsHandler(PixelContext context, IMapper mapper, IMemoryCache cache)
        {
            _context = context;
            _mapper = mapper;
            _cache = cache;
        }

        public async Task<MapStatsView> Handle(GetMapStatsQuery request, CancellationToken cancellationToken)
        {
            var records = await _context.Records.Where(rec => rec.MapName == request.Map.ToLower()).ToListAsync();
            return new MapStatsView
            {
                Map = request.Map,
                Progression = records.GroupBy(rec => rec.Created.ToString("yyyy-MM-dd", CultureInfo.CurrentCulture))
                    .ToDictionary(rec => rec.Key, rec => (int)rec.Min(rec => rec.Time))
            };
        }
    }
}