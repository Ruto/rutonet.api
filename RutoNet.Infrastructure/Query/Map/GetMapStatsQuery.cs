using MediatR;
using RutoNet.Core.ViewModels;

namespace RutoNet.Infrastructure.Query
{
    public class GetMapStatsQuery : IRequest<MapStatsView>
    {
        public string Map { get; set; }

        public GetMapStatsQuery(string map)
        {
            Map = map;
        }
    }
}