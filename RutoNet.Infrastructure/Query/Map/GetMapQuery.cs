using MediatR;
using RutoNet.Core.ViewModels;

namespace RutoNet.Infrastructure.Query
{
    public class GetMapQuery : IRequest<MapView>
    {
        public string Name { get; set; }

        public GetMapQuery(string searchTerm)
        {
            Name = searchTerm;
        }
    }
}