﻿using MediatR;
using RutoNet.Core.Entities;
using RutoNet.Core.ViewModels;

namespace RutoNet.Infrastructure.Query
{
    public class GetReplayQuery : IRequest<byte[]>
    {
        public RecordView Record { get; set; }

        public GetReplayQuery(RecordView record)
        {
            Record = record;
        }

    }
}