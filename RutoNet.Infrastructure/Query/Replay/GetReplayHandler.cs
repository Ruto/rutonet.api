﻿using Amazon.S3;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Query
{
    public class GetReplayHandler : IRequestHandler<GetReplayQuery, byte[]>
    {
        private readonly IAmazonS3 _s3;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly string _bucketName;
        private ILogger _logger;

        public GetReplayHandler(IAmazonS3 s3, IMediator mediator, IMapper mapper, ILogger<GetReplayHandler> logger, IConfiguration config)
        {
            _s3 = s3;
            _mediator = mediator;
            _mapper = mapper;
            _logger = logger;
            _bucketName = config["AWS:ReplayBucket"];
        }

        public async Task<byte[]> Handle(GetReplayQuery request, CancellationToken cancellationToken)
        {
            var key = GenerateFilename(request.Record);
            _logger.LogInformation("Querying S3 object: " + key);

            using var stream = await _s3.GetObjectStreamAsync(_bucketName, key, null);
            using var ms = new MemoryStream();
            await stream.CopyToAsync(ms);
            return ms.ToArray();
        }

        private static string GenerateFilename(RecordView rec)
        {
            return $"{rec.Map}_{rec.SteamID64}_{rec.Id}.replay";
        }
    }
}