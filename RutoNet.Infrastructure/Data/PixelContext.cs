using Microsoft.EntityFrameworkCore;
using RutoNet.Core.Entities;

namespace RutoNet.Infrastructure.Data
{
    public class PixelContext : DbContext
    {
        public DbSet<Map> Maps { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Record> Records { get; set; }
        public DbSet<PlayerMap> PlayerMapStats { get; set; }

        public PixelContext(DbContextOptions<PixelContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Record>(entity =>
            {
                entity.Property(record => record.Created)
                    .HasDefaultValueSql("NOW() AT TIME ZONE 'utc'");
            });

            modelBuilder.Entity<Map>(entity =>
            {
                entity.Property(map => map.Created)
                    .HasDefaultValueSql("NOW() AT TIME ZONE 'utc'");
                entity.Property(map => map.ReplayCount)
                    .HasDefaultValue(0);
            });

            modelBuilder.Entity<Player>(entity =>
            {
                entity.Property(player => player.Created)
                    .HasDefaultValueSql("NOW() AT TIME ZONE 'utc'");
            });

            modelBuilder.Entity<PlayerMap>(entity =>
            {
                entity.HasKey(table => new { table.SteamID64, table.Map });
                entity.Property(playerMap => playerMap.Created)
                    .HasDefaultValueSql("NOW() AT TIME ZONE 'utc'");
                entity.Property(playerMap => playerMap.TotalRecords)
                    .HasDefaultValue(0);
            });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSnakeCaseNamingConvention();
        }
    }
}