using MediatR;

namespace RutoNet.Infrastructure.Command
{
    public class IncrementReplayCountCommand : IRequest<int>
    {
        public string Map { get; set; }

        public IncrementReplayCountCommand(string map)
        {
            Map = map.ToLower();
        }
    }
}