using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Command
{
    public class CreateMapHandler : IRequestHandler<CreateMapCommand, MapView>
    {
        private readonly PixelContext _context;
        private readonly IMapper _mapper;

        public CreateMapHandler(PixelContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<MapView> Handle(CreateMapCommand request, CancellationToken cancellationToken)
        {
            var map = _mapper.Map<Core.Entities.Map>(request.CreateMapDTO);
            map.Name = map.Name.ToLower();
            if (!_context.Maps.Any(m => m.Name == map.Name))
            {
                await _context.Maps.AddAsync(map);
                await _context.SaveChangesAsync();
            }
            else
            {
                map = await _context.Maps.FirstOrDefaultAsync(m => m.Name == map.Name);
            }

            return _mapper.Map<MapView>(map);
        }
    }
}