using MediatR;
using RutoNet.Core.DTO;
using RutoNet.Core.ViewModels;

namespace RutoNet.Infrastructure.Command
{
    public class CreateMapCommand : IRequest<MapView>
    {
        public CreateMapDTO CreateMapDTO { get; set; }

        public CreateMapCommand(CreateMapDTO dto)
        {
            CreateMapDTO = dto;
        }
    }
}