using MediatR;
using RutoNet.Infrastructure.Data;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Command
{
    public class IncrementReplayCountHandler : IRequestHandler<IncrementReplayCountCommand, int>
    {
        private readonly PixelContext _context;

        public IncrementReplayCountHandler(PixelContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(IncrementReplayCountCommand request, CancellationToken cancellationToken)
        {
            var map = await _context.Maps.FindAsync(request.Map);
            if (map == null)
                return -1;

            _context.Attach(map);
            map.ReplayCount++;
            _context.Entry(map).Property(m => m.ReplayCount).IsModified = true;

            await _context.SaveChangesAsync();

            return map.ReplayCount;
        }
    }
}