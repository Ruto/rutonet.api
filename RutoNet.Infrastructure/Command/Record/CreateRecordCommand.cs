using MediatR;
using RutoNet.Core.DTO;
using RutoNet.Core.ViewModels;

namespace RutoNet.Infrastructure.Command
{
    public class CreateRecordCommand : IRequest<RecordView>
    {
        public CreateRecordDTO CreateRecordDTO { get; set; }

        public CreateRecordCommand(CreateRecordDTO dto)
        {
            CreateRecordDTO = dto;
        }
    }
}