using AutoMapper;
using MediatR;
using RutoNet.Core.DTO;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Command
{
    public class CreateRecordHandler : IRequestHandler<CreateRecordCommand, RecordView>
    {
        private readonly PixelContext _context;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public CreateRecordHandler(PixelContext context, IMediator mediator, IMapper mapper)
        {
            _context = context;
            _mediator = mediator;
            _mapper = mapper;
        }

        public async Task<RecordView> Handle(CreateRecordCommand request, CancellationToken cancellationToken)
        {
            var dto = request.CreateRecordDTO;
            var record = _mapper.Map<Core.Entities.Record>(dto);

            // Execute Create Map command
            var createMapCommand = new CreateMapCommand(new CreateMapDTO { Name = record.MapName });
            var map = await _mediator.Send(createMapCommand, cancellationToken);
            if (map == null)
                throw new Exception("Unable to create or find map");

            // Execute Update Player Command
            var updatePlayerCommand = new UpdatePlayerCommand(record.SteamID64, dto.PlayerName);
            var player = await _mediator.Send(updatePlayerCommand, cancellationToken);
            if (player == null)
                throw new Exception("Unable to update player");

            // Add Record
            await _context.Records.AddAsync(record, cancellationToken);
            await _context.SaveChangesAsync();

            // Execute Update PlayerMap
            var updateMapStatCommand = new UpdatePlayerRecordCommand
            {
                SteamID64 = dto.SteamID64,
                Map = map.Name,
                Record = record
            };
            await _mediator.Send(updateMapStatCommand);

            return _mapper.Map<RecordView>(record);
        }
    }
}