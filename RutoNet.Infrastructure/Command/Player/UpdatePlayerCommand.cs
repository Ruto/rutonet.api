using MediatR;
using RutoNet.Core.ViewModels;

namespace RutoNet.Infrastructure.Command
{
    public class UpdatePlayerCommand : IRequest<PlayerView>
    {
        public long SteamID64 { get; set; }
        public string Name { get; set; }

        public UpdatePlayerCommand(long steamId64, string name)
        {
            SteamID64 = steamId64;
            Name = name;
        }
    }
}