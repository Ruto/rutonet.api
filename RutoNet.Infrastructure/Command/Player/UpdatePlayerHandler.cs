using AutoMapper;
using MediatR;
using NodaTime;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Data;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Command
{
    public class UpdatePlayerHandler : IRequestHandler<UpdatePlayerCommand, PlayerView>
    {
        private readonly PixelContext _context;
        private readonly IMapper _mapper;

        public UpdatePlayerHandler(PixelContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PlayerView> Handle(UpdatePlayerCommand request, CancellationToken cancellationToken)
        {
            var existingPlayer = await _context.Players.FindAsync(request.SteamID64);
            if (existingPlayer == null)
            {
                existingPlayer = new Core.Entities.Player { SteamID64 = request.SteamID64, Name = request.Name };
                await _context.Players.AddAsync(existingPlayer);
            }
            else
            {
                _context.Attach(existingPlayer);
                existingPlayer.Name = request.Name;
                existingPlayer.Updated = SystemClock.Instance.GetCurrentInstant();
                _context.Entry(existingPlayer).Property(player => player.Name).IsModified = true;
                _context.Entry(existingPlayer).Property(player => player.Updated).IsModified = true;
            }
            await _context.SaveChangesAsync();
            return _mapper.Map<PlayerView>(existingPlayer);
        }
    }
}