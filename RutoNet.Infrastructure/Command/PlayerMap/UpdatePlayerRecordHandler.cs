using System.Threading;
using System.Threading.Tasks;
using MediatR;
using NodaTime;
using RutoNet.Core.Entities;
using RutoNet.Infrastructure.Data;

namespace RutoNet.Infrastructure.Command
{
    public class UpdatePlayerRecordHandler : IRequestHandler<UpdatePlayerRecordCommand, bool>
    {
        private readonly PixelContext _context;

        public UpdatePlayerRecordHandler(PixelContext context)
        {
            _context = context;
        }

        public async Task<bool> Handle(UpdatePlayerRecordCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.PlayerMapStats.FindAsync(request.SteamID64, request.Map);
            if (entity == null)
            {
                entity = new PlayerMap
                {
                    SteamID64 = request.SteamID64,
                    Map = request.Map,
                    PbId = request.Record.Id,
                    TotalRecords = 1,
                    Updated = SystemClock.Instance.GetCurrentInstant()
                };
                await _context.PlayerMapStats.AddAsync(entity);
            }
            else
            {
                _context.Attach(entity);
                entity.Updated = SystemClock.Instance.GetCurrentInstant();
                entity.TotalRecords++;
                var currentRecord = await _context.Records.FindAsync(entity.PbId);
                if (request.Record.Time <= currentRecord.Time)
                    entity.PbId = request.Record.Id;
            }
            await _context.SaveChangesAsync();
            return true;
        }
    }
}