using MediatR;
using RutoNet.Core.Entities;

namespace RutoNet.Infrastructure.Command
{
    public class UpdatePlayerRecordCommand : IRequest<bool>
    {
        public long SteamID64 { get; set; }
        public string Map { get; set; }
        public Record Record { get; set; }
    }
}