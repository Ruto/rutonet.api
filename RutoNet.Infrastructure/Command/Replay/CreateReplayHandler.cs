using Amazon.S3;
using MediatR;
using Microsoft.Extensions.Configuration;
using RutoNet.Core.DTO;
using RutoNet.Core.ViewModels;
using RutoNet.Infrastructure.Command.Replay;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace RutoNet.Infrastructure.Command
{
    public class CreateReplayHandler : IRequestHandler<CreateReplayCommand, RecordView>
    {
        private readonly IAmazonS3 _s3;
        private readonly IMediator _mediator;
        private readonly string _bucketName;

        public CreateReplayHandler(IAmazonS3 s3, IMediator mediator, IConfiguration config)
        {
            _s3 = s3;
            _mediator = mediator;
            _bucketName = config["AWS:ReplayBucket"];
        }

        public async Task<RecordView> Handle(CreateReplayCommand request, CancellationToken cancellationToken)
        {
            using var ms = new MemoryStream();
            await request.Stream.CopyToAsync(ms);
            ms.Position = 0;
            var replayFile = new ReplayFile(ms);

            // Execute Create Record command
            var dto = new CreateRecordDTO
            {
                SteamID64 = ConvertToSteamID64(replayFile.SteamId),
                PlayerName = replayFile.PlayerName,
                MapName = replayFile.MapName,
                Mode = (int)replayFile.Mode,
                Course = replayFile.Course,
                Time = (long)Math.Floor(replayFile.Time.TotalMilliseconds),
                Teleports = replayFile.TeleportsUsed
            };
            var createRecordCommand = new CreateRecordCommand(dto);
            var record = await _mediator.Send(createRecordCommand);

            using var newStream = new MemoryStream();
            replayFile.Write(newStream);
            newStream.Position = 0;
            await _s3.UploadObjectFromStreamAsync(_bucketName, GenerateFilename(record), newStream, null, cancellationToken);

            var incrementCommand = new IncrementReplayCountCommand(dto.MapName);
            await _mediator.Send(incrementCommand);

            return record;
        }

        private static long ConvertToSteamID64(int steamId32)
        {
            return steamId32 + 76561197960265728L;
        }

        private static string GenerateFilename(RecordView rec)
        {
            return $"{rec.Map}_{rec.SteamID64}_{rec.Id}.replay";
        }
    }
}