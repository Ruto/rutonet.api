using MediatR;
using RutoNet.Core.ViewModels;
using System.IO;

namespace RutoNet.Infrastructure.Command
{
    public class CreateReplayCommand : IRequest<RecordView>
    {
        public Stream Stream { get; set; }

        public CreateReplayCommand(Stream stream)
        {
            Stream = stream;
        }

        public CreateReplayCommand(byte[] data) : this(new MemoryStream(data))
        {
        }
    }
}