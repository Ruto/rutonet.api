using System.Text.Json;
using System.Text.RegularExpressions;

namespace RutoNet.Infrastructure.Policy
{
    public class SnakeCaseNamingPolicy : JsonNamingPolicy
    {
        public override string ConvertName(string name)
        {
            // https://github.com/Humanizr/Humanizer/blob/86e9d7d09016ad1fbf580f1fe3b0bd965080f8bf/src/Humanizer/InflectorExtensions.cs#L94
            // Don't need to install a whole package for this
            return Regex.Replace(
                        Regex.Replace(
                            Regex.Replace(name, @"([\p{Lu}]+)([\p{Lu}][\p{Ll}])", "$1_$2"), @"([\p{Ll}\d])([\p{Lu}])", "$1_$2"), @"[-\s]", "_").ToLower();
        }
    }
}